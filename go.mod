module gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment

go 1.20

require (
	github.com/BurntSushi/toml v1.2.1
	github.com/sirupsen/logrus v1.9.2
	github.com/stretchr/testify v1.7.0
	github.com/tliron/glsp v0.1.1
	github.com/tliron/kutil v0.1.56
	github.com/urfave/cli/v2 v2.25.3
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sourcegraph/jsonrpc2 v0.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.0.0-20221010170243-090e33056c14 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
