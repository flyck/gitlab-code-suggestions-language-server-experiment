#!/usr/bin/env bash

## Release binaries in the package registry and attach assets to release
SEMVER_PAT="[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*"
LATEST=$(egrep -o "## v${SEMVER_PAT}" CHANGELOG.md | tr -d ' #' | head -n 1)

[ "$LATEST" == "" ] && {
	echo "unable to extract latest version"
	exit 1
}

find ./dist -type f -name "gitlab-*" | while read -r binfile; do
	BASE=$(basename ${binfile})
        URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/generic/releases/${LATEST}/${BASE}"
	curl --fail --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "$URL" -o /dev/null --silent && {
		echo "${URL} already present"
		continue
	}
	
	echo "uploading '${binfile}' to '${URL}'"
	curl --fail --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --upload-file "$binfile" "$URL" || {
		echo "unable to upload ${URL}"
		exit 1
	}

	## attach released binary to (already existing) release
	curl --request POST \
	    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
	    --data name="$BASE" \
	    --data url="$URL" \
	    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${LATEST}/assets/links"
done

exit 0
