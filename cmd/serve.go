package cmd

import (
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/provider"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/server"
)

func RunServe(cli *cli.Context) error {
	srcdir := cli.String(server.LangSrvSrcDirParamName)
	name := cli.String(server.LangSrvNameParamName)
	host := cli.String(server.LangSrvHostParamName)
	token := cli.String(server.LangSrvGitLabAPITokenParamName)
	timeoutTime := cli.Uint(TimeoutFlagName)
	port := cli.Uint(server.LangSrvPortParamName)
	logFile := cli.String(LogFileFlagName)
	debug := cli.Bool(DebugFlagName)
	quiet := cli.Bool(QuietFlagName)
	gitlabBaseURL := cli.String(GitLabURLFlagName)

	if logFile != "" {
		f, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0o600)
		if err != nil {
			return fmt.Errorf("opening log file: %w", err)
		}

		log.SetOutput(f)
	}

	if debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("debugging enabled")
	}

	if quiet {
		log.SetLevel(log.WarnLevel)
	}

	if token == "" {
		return fmt.Errorf("no GitLab API Token specified")
	}

	log.Infof("Run GitLab API Code Suggestion language server")

	config, err := server.NewLspServerConfiguration(name, srcdir, host, token, 300, 300, port)
	if err != nil {
		return err
	}

	csp, err := provider.New(name, config.Token, "unknown", "0", provider.CodeSuggestionProviderOptions{
		Timeout:         time.Second * time.Duration(timeoutTime),
		RedactSensitive: cli.Bool(RedactSensitiveFlagName),
		BaseURL:         gitlabBaseURL,
	})
	if err != nil {
		return fmt.Errorf("initializing code suggestion provider: %w", err)
	}

	log.Info(config.String())
	log.Infof("timeout in seconds: %d", timeoutTime)

	ls := server.NewLspServer(config)
	ls.AddFeedbackProvider(name, csp)
	return ls.Start()
}
