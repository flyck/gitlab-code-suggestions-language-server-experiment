package server

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

const (
	LangSrvNameParamName  = "name"
	LangSrvNameEnvVarName = "LANGSRV_NAME"

	LangSrvHostParamName  = "host"
	LangSrvHostEnvVarName = "LANGSRV_HOST"

	LangSrvSrcDirParamName  = "srcdir"
	LangSrvSrcDirEnvVarName = "LANGSRV_SRC_DIR"

	LangSrvGitLabAPITokenParamName  = "gitlab-api-token"
	LangSrvGitLabAPITokenEnvVarName = "LANGSRV_GITLAB_API_TOKEN"

	LangSrvPortParamName  = "port"
	LangSrvPortEnvVarName = "LANGSRV_PORT"

	baseURLParamName       = "gitlab-url"
	baseURLParamEnvVarName = "LANGSRV_GITLAB_URL"
)

// commonly used options/parameters/flags
// TODO: we probably have to parameterize the env variables in case we want to
// run multiple language servers
var nameParam = &cli.StringFlag{
	Name:    LangSrvNameParamName,
	Usage:   "source dir that contains the source code to run the analysis on.",
	Value:   "",
	EnvVars: []string{LangSrvNameEnvVarName},
}

var sourceDirParam = &cli.StringFlag{
	Name:    LangSrvSrcDirParamName,
	Usage:   "source dir that contains the source code to run the analysis on.",
	Value:   "",
	EnvVars: []string{LangSrvSrcDirEnvVarName},
}

var hostParam = &cli.StringFlag{
	Name:    LangSrvHostParamName,
	Usage:   "host through which the LSP server is reachable.",
	Value:   "",
	EnvVars: []string{LangSrvHostEnvVarName},
}

var gitlabAPITokenParam = &cli.StringFlag{
	Name:     LangSrvGitLabAPITokenParamName,
	Usage:    "GitLab API token",
	Value:    "",
	EnvVars:  []string{LangSrvGitLabAPITokenEnvVarName},
	FilePath: token.GitlabAccessTokenFile(),
}

var portParam = &cli.IntFlag{
	Name:    LangSrvPortParamName,
	Usage:   "port through which the LSP server is reachable.",
	Value:   6789,
	EnvVars: []string{LangSrvPortEnvVarName},
}

var baseURLParam = &cli.StringFlag{
	Name:    baseURLParamName,
	Usage:   "The base URL for GitLab",
	Value:   "https://gitlab.com",
	EnvVars: []string{baseURLParamEnvVarName},
}
