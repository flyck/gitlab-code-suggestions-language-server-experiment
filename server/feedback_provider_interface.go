package server

import (
	"fmt"

	protocol "github.com/tliron/glsp/protocol_3_16"
)

type Severity int

const (
	SeverityHigh = iota
	SeverityMedium
	SeverityLow
)

type DocumentAction int

const (
	DocumentOpen = iota
	DocumentSave
	DocumentChange
)

// Coordinate describes the position of a finding
type Coordinate struct {
	StartLine, EndLine, StartCol, EndCol int
}

// CodeDiagnostic objects are useful to highlight issues in the IDE.
// If you know how to fix an issue, you can provide a Fix with the Diagnostic.
type CodeDiagnostic struct {
	Origin, // where does the diagnostic originate from
	Message, // the message to be displayed in the ide
	File string // the file that contains the issue
	Severity   Severity   // severity of the issue (most IDEs will provide color codes)
	Coordinate Coordinate // source code coordiates of the issue
	Fix        *CodeFix   // Fix (Optional)
}

// CodeSuggesion objects are useful to provide code suggestions in the IDE.
// Most IDEs will provide a drop-down menu from which the user can make a
// selection.
type CodeSuggesion struct {
	Label       string
	Content     string
	Description string
}

func NewCodeSuggestion(label, content, description string) *CodeSuggesion {
	return &CodeSuggesion{
		Label:       label,
		Content:     content,
		Description: description,
	}
}

func (s *CodeSuggesion) ToCompletionItem() protocol.CompletionItem {
	return protocol.CompletionItem{
		Preselect:  boolPtr(true),
		Label:      s.Label,
		InsertText: stringPtr(s.Content),
		Detail:     stringPtr(s.Description),
	}
}

// CodeFix describes a fix for a given issue and is, hence, always attached to
// a CodeDiagnostic object.
type CodeFix struct {
	Hint        string // A small description to describe the fix.
	Replacement string // The actual replacement for the code captured by the Diagnostic.
}

// CursorContext defines the text before and after the cursors
// the window width
type CursorContext struct {
	Before, After string
}

func (c *CursorContext) String() string {
	return fmt.Sprintf("Cursor Context\nBefore: %s\nAfter: %s", c.Before, c.After)
}

func (c *CursorContext) Empty() bool {
	return c.Before == "" && c.After == ""
}

// FeedBackProvider is the (only) interface new language servers have to
// implement.
type FeedBackProvider interface {
	// Set the client name and version to be used in user-agent string
	SetClientNameAndVersion(clientName, clientVersion string)
	// ProvideDiagnosticsForFile is a callback to pass diagnostics to the
	// language server. The results will be highlighted in the IDE.
	ProvideDiagnosticsForFile(file string, action DocumentAction) ([]*CodeDiagnostic, error)
	// ProvideDiagnosticsForContent is identical to
	// ProvideDiagnosticsForFile with the only difference that it runs on
	// the content and captures in-memory change.
	ProvideDiagnosticsForContent(filePath, content string, action DocumentAction) ([]*CodeDiagnostic, error)
	ProvideCodeSuggesions(filePath string, context *CursorContext) ([]*CodeSuggesion, error)
}
