package provider

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/api"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/redact"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/server"
)

const (
	labelPrefix = "GitLab"
)

// CodeSuggestionProviderOptions for [CodeSuggestionProvider].
type CodeSuggestionProviderOptions struct {
	Timeout         time.Duration // Request timeout.
	RedactSensitive bool          // Redact sensitive content in code context.
	BaseURL         string
}

type CodeSuggestionProvider struct {
	name, token               string
	clientName, clientVersion string
	timeout                   time.Duration
	redactSensitive           bool
	redactor                  *redact.Engine
	client                    *api.GitLabClient
}

func New(name, token, clientName, clientVersion string, opts CodeSuggestionProviderOptions) (*CodeSuggestionProvider, error) {
	provider := &CodeSuggestionProvider{
		name:            name,
		token:           token,
		clientName:      clientName,
		clientVersion:   clientVersion,
		timeout:         opts.Timeout,
		redactSensitive: opts.RedactSensitive,
		client:          api.NewClient(clientName, clientVersion, token, opts.BaseURL, opts.Timeout),
	}

	if !opts.RedactSensitive {
		return provider, nil
	}

	rules, err := redact.LoadGitleaksRules()
	if err != nil {
		return nil, fmt.Errorf("loading secrets detection rules: %w", err)
	}

	provider.redactor = redact.New(redact.WithGitleaksRules(rules))

	return provider, nil
}

func (csp *CodeSuggestionProvider) SetClientNameAndVersion(clientName, clientVersion string) {
	csp.clientName = clientName
	csp.clientVersion = clientVersion
	csp.client.SetClientNameAndVersion(clientName, clientVersion)
}

func (csp *CodeSuggestionProvider) ProvideDiagnosticsForContent(filePath, content string, action server.DocumentAction) ([]*server.CodeDiagnostic, error) {
	return []*server.CodeDiagnostic{}, nil
}

func (csp *CodeSuggestionProvider) ProvideDiagnosticsForFile(file string, action server.DocumentAction) ([]*server.CodeDiagnostic, error) {
	return []*server.CodeDiagnostic{}, nil
}

func (csp *CodeSuggestionProvider) ProvideCodeSuggesions(filePath string, cursorContext *server.CursorContext) ([]*server.CodeSuggesion, error) {
	log.Debugf("Provide Code Suggestion %s", cursorContext.String())

	ctx, cancel := context.WithTimeout(context.Background(), csp.timeout)
	defer cancel()

	if csp.redactSensitive {
		cursorContext.Before, _ = csp.redactor.Redact(cursorContext.Before)
		cursorContext.After, _ = csp.redactor.Redact(cursorContext.After)
	}

	suggestions, err := csp.client.GetCodeSuggestions(ctx, filePath, cursorContext.Before, cursorContext.After)
	if err != nil {
		return nil, fmt.Errorf("fetching code suggestions from API: %w", err)
	}

	ret := []*server.CodeSuggesion{}
	for id, choice := range suggestions.Choices {
		label := fmt.Sprintf("%s Suggestion %d", labelPrefix, id+1)
		ret = append(ret, server.NewCodeSuggestion(label, choice.Text, choice.Text))
	}
	return ret, nil
}
