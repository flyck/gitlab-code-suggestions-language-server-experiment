package api

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIntegrationGetCodeSuggestionsToken(t *testing.T) {
	token := os.Getenv("LANGSRV_TEST_GITLAB_TOKEN")
	if token == "" {
		t.Skip(`LANGSRV_TEST_GITLAB_TOKEN not set, skipping integration test`)
	}

	c := NewClient("TestIntegrationGetCodeSuggestionsToken", "0.0", token, "https://gitlab.com", time.Second*60)

	completionToken, err := c.GetCodeSuggestionsToken(context.Background())
	require.NoError(t, err)
	require.NotNil(t, completionToken)
	require.NotEmpty(t, completionToken.AccessToken)
}

func TestIsTokenValid(t *testing.T) {
	tt := []struct {
		description string
		createdAt   int64
		expected    bool
	}{
		{
			description: "when the token hasn't expired returns true",
			createdAt:   time.Now().Unix(),
			expected:    true,
		},
		{
			description: "when the token has expired returns false",
			createdAt:   time.Now().Unix() - 180,
			expected:    false,
		},
	}

	c := NewClient("TestIsTokenValid", "0.0", "123", "https://gitlab.com", time.Second*60)

	for _, test := range tt {
		t.Run(test.description, func(t *testing.T) {
			c.currentToken = &codeSuggestionsToken{
				AccessToken: "1234",
				ExpiresIn:   60,
				CreatedAt:   test.createdAt,
			}

			require.Equal(t, test.expected, c.isTokenValid())
		})
	}
}
