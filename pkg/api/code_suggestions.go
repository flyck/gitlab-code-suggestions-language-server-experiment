package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

type CurrentFile struct {
	FileName           string `json:"file_name"`
	ContentAboveCursor string `json:"content_above_cursor"`
	ContentBelowCursor string `json:"content_below_cursor"`
}

type Request struct {
	PromptVersion int         `json:"prompt_version"`
	ProjectPath   string      `json:"project_path"`
	ProjectID     int         `json:"project_id"`
	CurrentFile   CurrentFile `json:"current_file"`
}

func (client *GitLabClient) GetCodeSuggestions(ctx context.Context, filePath, textBefore, textAfter string) (*CodeSuggestionResponse, error) {
	err := client.ensureValidToken(ctx)
	if err != nil {
		return nil, err
	}

	r := newGitLabCodeSuggestionRequest(filePath, textBefore, textAfter)
	rs, err := r.String()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", "https://codesuggestions.gitlab.com/v2/completions", strings.NewReader(rs))
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Gitlab-Authentication-Type", "oidc")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", client.currentToken.AccessToken))

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Errorf("Error: %d", resp.StatusCode)
		return nil, fmt.Errorf("error calling API: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	suggestions, err := newCodeSuggestionResponse(string(body))
	if err != nil {
		return nil, err
	}

	return suggestions, nil
}

func newGitLabCodeSuggestionRequest(fileName, contentBefore, contextAfter string) Request {
	return Request{
		PromptVersion: 1,
		ProjectPath:   "",
		ProjectID:     -1,
		CurrentFile: CurrentFile{
			FileName:           fileName,
			ContentAboveCursor: contentBefore,
			ContentBelowCursor: contextAfter,
		},
	}
}

func (r Request) String() (string, error) {
	bytes, err := json.Marshal(r)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

type CodeSuggestionResponseChoice struct {
	Text string `json:"text"`
}

type CodeSuggestionResponse struct {
	Choices []CodeSuggestionResponseChoice `json:"choices"`
}

func newCodeSuggestionResponse(response string) (*CodeSuggestionResponse, error) {
	resp := &CodeSuggestionResponse{}
	err := json.Unmarshal([]byte(response), resp)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
