package api

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIntegrationGetCodeSuggestions(t *testing.T) {
	token := os.Getenv("LANGSRV_TEST_GITLAB_TOKEN")
	if token == "" {
		t.Skip(`LANGSRV_TEST_GITLAB_TOKEN not set, skipping integration test`)
	}

	c := NewClient("TestIntegrationGetCodeSuggestions", "0.0", token, "https://gitlab.com", time.Second*60)

	completion, err := c.GetCodeSuggestions(context.Background(), "main.py", "def add(", ")")
	require.NoError(t, err)
	require.NotNil(t, completion)
	require.NotEmpty(t, completion.Choices[0].Text)
}
