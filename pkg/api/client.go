package api

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type Client interface {
	GetCodeSuggestions(ctx context.Context, filePath, textBefore, textAfter string) (*CodeSuggestionResponse, error)
	GetCodeSuggestionsToken(ctx context.Context) (*codeSuggestionsToken, error)
}

type GitLabClient struct {
	clientName, clientVersion string
	httpClient                *http.Client
	currentToken              *codeSuggestionsToken
	baseURL                   string
}

func NewClient(clientName, clientVersion, token, baseURL string, timeout time.Duration) *GitLabClient {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConns:          1,
			IdleConnTimeout:       timeout,
			ResponseHeaderTimeout: timeout,
			DisableKeepAlives:     true,
		},
	}

	gitlabClient := &GitLabClient{
		baseURL:       baseURL,
		clientName:    clientName,
		clientVersion: clientVersion,
		httpClient:    client,
	}

	client.Transport = tokenTransport{
		Transport: client.Transport,
		Token:     token,
		Client:    gitlabClient,
	}

	return gitlabClient
}

type tokenTransport struct {
	Transport http.RoundTripper
	Token     string
	Client    *GitLabClient
}

func (att tokenTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.Header.Get("Authorization") == "" {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", att.Token))
	}

	userAgent := fmt.Sprintf("%s (%s:%s)",
		"code-completions-language-server-experiment",
		att.Client.clientName,
		att.Client.clientVersion)

	req.Header.Add("User-Agent", userAgent)

	return att.Transport.RoundTrip(req)
}

func (client *GitLabClient) SetClientNameAndVersion(clientName, clientVersion string) {
	log.Debugf("GitLabClient.SetClientNameAndVersion: %s, %s", clientName, clientVersion)
	client.clientName = clientName
	client.clientVersion = clientVersion
}
