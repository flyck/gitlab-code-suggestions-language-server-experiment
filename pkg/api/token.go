package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

type codeSuggestionsToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
	CreatedAt   int64  `json:"created_at"`
}

type PersonalAccessToken struct {
	Name   string   `json:"name"`
	Scopes []string `json:"scopes"`
	Active bool     `json:"active"`
}

var suitableScopes = []string{"read_api", "api"}

func (p PersonalAccessToken) isActive() bool {
	return p.Active
}

func (p PersonalAccessToken) hasSuitableScopes() bool {
	for _, i := range p.Scopes {
		for _, f := range suitableScopes {
			if i == f {
				return true
			}
		}
	}

	return false
}

func (p PersonalAccessToken) IsValidExplaination() string {
	if p.isActive() {
		if p.hasSuitableScopes() {
			return fmt.Sprintf("✔ Token '%s' has scope(s) '%s'", p.Name, p.scopesFormatted())
		} else {
			return fmt.Sprintf("✖ Token '%s' has scope(s) '%s' but needs '%v'", p.Name, p.scopesFormatted(), p.SuitableScopesFormatted())
		}
	} else {
		return fmt.Sprintf("✖ Token '%s' is not active", p.Name)
	}
}

func (p PersonalAccessToken) scopesFormatted() string {
	return strings.Join(p.Scopes, ", ")
}

func (p PersonalAccessToken) SuitableScopesFormatted() string {
	return strings.Join(suitableScopes, " or ")
}

func (client *GitLabClient) GetTokenSelf() (*PersonalAccessToken, error) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/v4/personal_access_tokens/self", client.baseURL), nil)
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)
	res, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected %s response when acquiring token (response body: %q)", res.Status, string(body))
	}

	var p PersonalAccessToken
	err = json.Unmarshal(body, &p)
	if err != nil {
		return nil, err
	}

	return &p, nil
}

func (client *GitLabClient) isTokenValid() bool {
	if client.currentToken != nil {
		currentTimeInUnix := time.Now().Unix()
		if client.currentToken.CreatedAt+client.currentToken.ExpiresIn >= currentTimeInUnix {
			return true
		}
	}

	return false
}

func (client *GitLabClient) ensureValidToken(ctx context.Context) error {
	if !client.isTokenValid() {
		token, err := client.GetCodeSuggestionsToken(ctx)
		if err != nil {
			return err
		}
		client.currentToken = token
	}

	return nil
}

func (client *GitLabClient) GetCodeSuggestionsToken(ctx context.Context) (*codeSuggestionsToken, error) {
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/v4/code_suggestions/tokens", client.baseURL), nil)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	res, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("unexpected %s response when acquiring token (response body: %q)", res.Status, string(body))
	}

	var token codeSuggestionsToken
	err = json.Unmarshal(body, &token)
	if err != nil {
		return nil, err
	}

	return &token, nil
}
