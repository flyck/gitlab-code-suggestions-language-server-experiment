package redact_test

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/redact"
)

// expectedGitleaksRules in the Gitleaks TOML configuration.
const expectedGitleaksRules = 149

func TestLoadGitleaksRules(t *testing.T) {
	rules, err := redact.LoadGitleaksRules()
	require.NoError(t, err)

	require.Len(t, rules, expectedGitleaksRules)

	for i, rule := range rules {
		require.NotNil(t, rule.CompiledRegex, "expected rule[%d].CompiledRegex to not be nil", i)
	}
}

func TestGitleaksRule_Redact(t *testing.T) {
	rule := &redact.GitleaksRule{
		CompiledRegex: regexp.MustCompile(`ba(r|z)`),
	}

	redacted, match := rule.Redact("foo bar baz")

	require.Equal(t, "foo *** ***", redacted)
	require.True(t, match)
}

func TestGitleaksRule_Redact_SecretGroup(t *testing.T) {
	rule := &redact.GitleaksRule{
		CompiledRegex: regexp.MustCompile(`supersecret=([a-f0-9]+)`),
		SecretGroup:   1,
	}

	redacted, match := rule.Redact("[config]\nsupersecret=deadbeefdeadbeef")

	require.Equal(t, "[config]\nsupersecret=****************", redacted)
	require.True(t, match)
}

func TestGitleaksRule_Redact_NoMatch(t *testing.T) {
	rule := &redact.GitleaksRule{
		CompiledRegex: regexp.MustCompile(`ba(r|z)`),
	}

	redacted, match := rule.Redact("Hello World")

	require.Equal(t, "Hello World", redacted)
	require.False(t, match)
}

func TestGitleaksRule_Redact_Keywords(t *testing.T) {
	rule := &redact.GitleaksRule{
		CompiledRegex: regexp.MustCompile(`ba(r|z)`),
		Keywords:      []string{"World"},
	}

	redacted, match := rule.Redact("Hello foo bar baz")

	require.Equal(t, "Hello foo bar baz", redacted)
	require.False(t, match)

	redacted, match = rule.Redact("Hello World foo bar baz")

	require.Equal(t, "Hello World foo *** ***", redacted)
	require.True(t, match)
}
