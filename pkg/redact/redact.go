package redact

// Redactor of sensitive content.
type Redactor interface {
	// Redact is expected to return a version of given string with any
	// sensitive values removed or masked. The `match` return value should be
	// true if a sensitive value was redacted, or false otherwise.
	Redact(raw string) (redacted string, match bool)
}

// Engine redacts sensitive content in code and other textual data.
type Engine struct {
	redactors []Redactor
}

// New Redactor for redacting sensitive content.
func New(opts ...Option) *Engine {
	r := &Engine{}

	for _, opt := range opts {
		opt(r)
	}

	return r
}

// Redact sensitive content in a string.
//
// Uses configured redactors to redact secrets and other sensitive content in
// the raw string and returns a redacted version. The `match` return value
// will be true if a value was redacted, and false otherwise.
//
// Panics if engine has not been configured with any redactors.
func (e *Engine) Redact(raw string) (string, bool) {
	if len(e.redactors) == 0 {
		panic("redactor engine has not been configured with any redactors")
	}

	redacted := raw
	match := false

	for _, redactor := range e.redactors {
		var matched bool

		redacted, matched = redactor.Redact(redacted)
		if matched {
			match = true
		}
	}

	return redacted, match
}

// Option configures an [Engine].
type Option func(*Engine)

// WithRedactors configures an [Engine] to redact sensitive content with given
// redactors.
func WithRedactors(redactors ...Redactor) Option {
	return func(e *Engine) {
		e.redactors = append(e.redactors, redactors...)
	}
}

// WithGitleaksRules configures an [Engine] to redact potential secrets using
// Gitleaks detection rules.
func WithGitleaksRules(rules []*GitleaksRule) Option {
	return func(e *Engine) {
		for _, r := range rules {
			e.redactors = append(e.redactors, r)
		}
	}
}
