package redact_test

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/redact"
)

func TestEngine_Redact(t *testing.T) {
	redactor := redact.New(redact.WithRedactors(newTestRedactor("super"), newTestRedactor("secret")))
	redacted, match := redactor.Redact("Hello super World secret")

	require.Equal(t, "Hello ***** World ******", redacted)
	require.True(t, match)
}

func TestEngine_Redact_NoMatch(t *testing.T) {
	redactor := redact.New(redact.WithRedactors(newTestRedactor("super"), newTestRedactor("secret")))
	redacted, match := redactor.Redact("Hello World")

	require.Equal(t, "Hello World", redacted)
	require.False(t, match)
}

func TestEngine_Redact_PanicWhenNoRedactors(t *testing.T) {
	redactor := redact.New()

	require.PanicsWithValue(t, "redactor engine has not been configured with any redactors", func() {
		redactor.Redact("Oops!")
	})
}

func TestEngine_Redact_WithGitleaksRules(t *testing.T) {
	rules, err := redact.LoadGitleaksRules()
	require.NoError(t, err)

	redactor := redact.New(redact.WithGitleaksRules(rules))
	raw := string(readFile(t, "testdata/gitleaks-test.txt"))
	expected := string(readFile(t, "testdata/gitleaks-test-redacted.txt"))

	redacted, match := redactor.Redact(raw)
	require.Equal(t, expected, redacted)
	require.True(t, match)
}

type testRedactor struct {
	replace string
}

func (r *testRedactor) Redact(raw string) (string, bool) {
	if r.replace == "" {
		return raw, false
	}

	redacted := strings.ReplaceAll(raw, r.replace, strings.Repeat("*", len(r.replace)))

	return redacted, raw != redacted
}

func newTestRedactor(replace string) redact.Redactor {
	return &testRedactor{replace}
}

func readFile(t *testing.T, name string) []byte {
	t.Helper()

	data, err := os.ReadFile(name)
	require.NoError(t, err)

	return data
}
