package token_test

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

func TestRunGitlabTokenRegister(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	mockStdin(t, testPAT+"\n")

	require.NoError(t, token.RunRegister())

	tokenFile := filepath.Join(cfgDir, "gitlab-ls", "token")

	require.FileExists(t, tokenFile)

	contents, err := os.ReadFile(tokenFile)
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func mockStdin(t *testing.T, data string) {
	t.Helper()

	tmpStdin, err := os.CreateTemp("", "gitlab-ls-test-stdin")
	require.NoError(t, err)

	_, err = tmpStdin.WriteString(data)
	require.NoError(t, err)

	_, err = tmpStdin.Seek(0, 0)
	require.NoError(t, err)

	origStdin := os.Stdin

	t.Cleanup(func() {
		os.Stdin = origStdin
		os.Remove(tmpStdin.Name())
	})

	os.Stdin = tmpStdin
}

// mockUserConfigDir creates a new temporary directory and overrides $HOME and
// $XDG_CONFIG_HOME environment variables for the test with the tempdir
// location. This prevents tests from overwriting the actual access token file
// in a user's configuration directory returned by [os.UserConfigDir].
//
// Returns path to mocked user configuration directory.
func mockUserConfigDir(t *testing.T) string {
	t.Helper()

	tmpHome := t.TempDir()

	t.Setenv("HOME", tmpHome)
	t.Setenv("XDG_CONFIG_HOME", tmpHome)

	cfgDir, err := os.UserConfigDir()
	require.NoError(t, err)

	if !strings.HasPrefix(cfgDir, tmpHome) {
		panic(fmt.Errorf("os.UserConfigDir did not return path within mock home directory %s: %s", tmpHome, cfgDir))
	}

	require.NoError(t, os.MkdirAll(cfgDir, 0o755))

	return cfgDir
}
