package token_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/api"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

type httpResponseWriterFn func(w http.ResponseWriter)

func TestRunCheck(t *testing.T) {
	ts := setupPATSelfServer(func(w http.ResponseWriter) {
		response := fmt.Sprintf(`{ "name": "Test Token", "scopes": ["api"], "active": %v }`, true)
		_, err := w.Write([]byte(response))
		if err != nil {
			t.Fatal(err)
		}
	})
	defer ts.Close()

	pat, err := token.RunCheck("fake-token", ts.URL)

	require.NoError(t, err)
	require.IsType(t, &api.PersonalAccessToken{}, pat)
}

func TestRunCheckError(t *testing.T) {
	ts := setupPATSelfServer(func(w http.ResponseWriter) {
		_, err := w.Write([]byte{})
		if err != nil {
			t.Fatal(err)
		}
	})
	defer ts.Close()

	pat, err := token.RunCheck("fake-token", ts.URL)

	require.EqualError(t, err, "unexpected end of JSON input")
	require.Nil(t, pat)
}

func setupPATSelfServer(f httpResponseWriterFn) *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f(w)
	}))

	return ts
}
