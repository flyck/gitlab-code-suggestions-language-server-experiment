package token

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func RunRegister() error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Printf("Enter GitLab access token: ")

	t, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("reading GitLab access token from stdin: %w", err)
	}

	t = strings.TrimSpace(t)

	if err := WriteGitlabAccessToken(t); err != nil {
		return err
	}

	fmt.Printf("✔ wrote GitLab access token to %s\n", GitlabAccessTokenFile())

	return nil
}
