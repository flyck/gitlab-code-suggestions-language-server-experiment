package token

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
)

const (
	accessTokenDirPerms  = 0o755
	accessTokenFilePerms = 0o600
)

var gitlabAccessTokenRegexp = regexp.MustCompile(`^glpat-[0-9a-zA-Z\-\_]{20}$`)

// ErrInvalidAccessToken is returned by [WriteAccessToken] if token does not
// look like a valid GitLab access token.
var ErrInvalidAccessToken = errors.New("invalid GitLab access token")

func GitlabAccessTokenFile() string {
	if filename := os.Getenv("LANGSRV_GITLAB_API_TOKEN_FILE"); filename != "" {
		return filename
	}

	cfgDir, err := os.UserConfigDir()
	if err != nil {
		// Error from UserConfigDir function is very rare, so we panic if we
		// encounter this to simplify the usage of this function the majority of
		// the time.
		panic(fmt.Errorf("determining user configuration directory: %w", err))
	}

	return filepath.Join(cfgDir, "gitlab-ls", "token")
}

// WriteGitlabAccessToken to a file inside current user's configuration directory.
//
// If the `LANGSRV_GITLAB_API_TOKEN_FILE` environment variable it set, it will
// use that as file path for the access token file.
func WriteGitlabAccessToken(t string) error {
	if !gitlabAccessTokenRegexp.MatchString(t) {
		return ErrInvalidAccessToken
	}

	filename := GitlabAccessTokenFile()
	dir := filepath.Dir(filename)

	if err := os.MkdirAll(dir, accessTokenDirPerms); err != nil {
		return fmt.Errorf("creating directory for access token file %s: %w", dir, err)
	}

	if err := os.WriteFile(filename, []byte(t), accessTokenFilePerms); err != nil {
		return fmt.Errorf("writing access token to %s: %w", filename, err)
	}

	return nil
}
