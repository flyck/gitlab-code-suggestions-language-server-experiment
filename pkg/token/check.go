package token

import (
	"time"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/api"
)

func RunCheck(token, gitlabBaseURL string) (*api.PersonalAccessToken, error) {
	// FIXME: Use real clientName and clientVersion
	gitlabCLient := api.NewClient("unknown", "0", token, gitlabBaseURL, time.Second*60)
	pat, err := gitlabCLient.GetTokenSelf()
	if err != nil {
		return nil, err
	}

	return pat, nil
}
