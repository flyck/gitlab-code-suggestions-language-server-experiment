gitlab-code-suggestions-language-server-experiment Changelog

## v0.14.0
- Note Code Suggestions (Beta) in Emacs configuration (!36)
- Note Code Suggestions (Beta) in Sublime Text configuration (!35)
- Add `--version` support (!31)
- Add lefthook support (!34)
- New token package (!33)
- Add 'token check' command (!32)

## v0.13.0
- Use client name and version in user-agent string (!28)

## v0.12.0
- Add support for JWT authentication for Code Suggestions

## v0.11.0
- Merge the `lang-server-go` repo into this one (!24)
- Drop dependency on Ristretto (!25)

## v0.10.1
- Added lint and fixed linting issues (!23)
- Added code coverage report (!23)
- Added .tool-versions (!23)
- Updated CI for linting and code coverage (!23)

## v0.10.0
- Added `--log-file` flag to log out to a file (!22)
- Added `--debug` flag to output debugging information (!22)
- Added `--quiet` flag to only output warnings and errors (!22)
- **BREAKING CHANGE:** Some flag environment variable names have changed due to a refactor (!22)

## v0.9.0
- Added redaction of sensitive content in code context (!19)

## v0.8.4
- Included a standalone plug&play `init.vim` for NeoVim
- Restructured documentation (dedicated md file per editor/IDE)

## v0.8.3
- Added Makefile to project (!15)
- Used standard go conventions for directory names cmd, pkg, etc
- Moved GitLab api to its own package
- Moved Provider to its own package
- Moved images to docs directory
- Made minor spelling corrections in Readme.md

## v0.8.3
- Dedicated configuration files with standalone NeoVim config (!16)

## v0.8.2
- Use filter in deploy script (!17)
- Add windows build to the deploy script; switch to goreleaser (!14)

## v0.8.1
Changed module path (!13)

## v0.8.0
Added command to register GitLab access token (!12)

## v0.7.0
Bump lang-server-go version (!11)

## v0.6.0
Attach binary assets to release (!8)

## v0.5.0
Removed submodule (!7)

## v0.4.0
Updated the API with the new CursorContext (!5)

## v0.3.0
Bump lang-server-go dependency version (!3)

## v0.2.0
Sensible default settings, bumped lang-server-go to v0.2.0 (!2)

## v0.1.0
Initial release (!1)
