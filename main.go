package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/cmd"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/server"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

// https://goreleaser.com/cookbooks/using-main.version/
var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	app := cli.NewApp()
	app.Name = "gitlab-code-suggestions-language-server-experiment"
	app.Usage = "GitLab AI Code Suggestions language server"
	app.Version = versionFormatted()

	app.Commands = []*cli.Command{
		{
			Name:    "serve",
			Aliases: nil,
			Usage:   "run language server",
			Action:  cmd.RunServe,
			Flags:   append(server.ServeFlags(), cmd.ServeFlags()...),
		},
		{
			Name:    "gitlab-token",
			Aliases: []string{"token"},
			Usage:   "commands for GitLab access token",
			Subcommands: []*cli.Command{
				{
					Name:    "register",
					Aliases: []string{"set", "save"},
					Usage:   "register GitLab access token",
					Action:  cmd.RunGitlabTokenRegister,
				},
				{
					Name:   "check",
					Usage:  "check GitLab access token",
					Action: cmd.RunGitlabTokenCheck,
					Flags:  token.TokenCheckFlags(),
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func versionFormatted() string {
	if version == "dev" {
		return version
	} else {
		return fmt.Sprintf("v%s-%s (built %s)", version, commit[0:8], date)
	}
}
