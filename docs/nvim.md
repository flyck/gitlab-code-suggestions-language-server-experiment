# NeoVim Configuration

![](./nvim.gif)

[[_TOC_]]

## General Configuration Instructions

NeoVim has [builtin support for LSP](https://neovim.io/doc/user/lsp.html).
NeoVim custom language servers can be easily integrated by putting the Lua
code below into your `init.lua` file. The configuration below assumes that 
we are using the [`nvim-lspconfig`](https://github.com/neovim/nvim-lspconfig).

Since there (Neo)Vim supports a variety of different LSP/completion plugins 
written in Lua/VimScript/Vim9Script, you may have to tweak the the configuration 
according to your concrete setup.

```lua
local capabilities = vim.lsp.protocol.make_client_capabilities()
-- ...
local cmd = {
  'opt/gitlab-code-suggestions-language-server-experiment',
  'serve',
  '--name',
  'gitlab-code-suggestions-language-server-experiment',
  '--srcdir',
  vim.fn.getcwd(),
  '--timeout-seconds',
  '20',
}

local configs = require 'lspconfig.configs'
local util = require 'lspconfig.util'

if not configs.gitlab_ai_code_suggestions then
  configs.gitlab_ai_code_suggestions = {
    default_config = {
      cmd = cmd,
      -- the files fro which the language server is going to be activated
      filetypes = {'python', 'ruby'},
      root_dir = function(fname)
        return util.find_git_ancestor(fname)
      end,
      settings = {},
    },
  }
end

configs.gitlab_ai_code_suggestions.setup{
  capabilities = capabilities
}
```

## Standalone Configuration

If you prefer to start with a self-contained, plug&play NeoVim configuration, a sample `init.lua` can be downloaded from [here](./init.lua).
Once you place the `init.lua` into your `$HOME/.config/nvim` directory, it will automatically setup a basic NeoVim configuration with GitLab AI Code suggestions enabled.

When you start NeoVim for the first time, all dependencies will be downloaded automatically. After restarting NeoVim, you have to invoke the  command `:GitLabRegisterToken <gitlab-api-token>` within Neovim which will be confirmed with the message `token registered`.When opening a Ruby or Python file, you should be able to see GitLab AI code suggestions when pressing `<CTRL>+<n>` in the NeoVim.

![](./nvim_complete.gif)
