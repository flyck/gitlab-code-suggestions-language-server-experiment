# Code Suggestions (Beta)

Write code more efficiently by using generative AI to suggest code while you’re developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-in-vs-code)

This feature is in
[Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta)
Code Suggestions is a generative artificial intelligence (AI) model. GitLab currently leverages [Google Cloud’s Vertex AI Codey API models](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview)

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Google Vertex AI Codey APIs Data Governance](https://cloud.google.com/vertex-ai/docs/generative-ai/data-governance)

## Sublime Text configuration for Code Suggestions

![](./sublime.gif)

To set up Code Suggestions in Sublime Text:

1. Follow the [Getting Started instructions](https://gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment#user-documentation). Make sure you have the `gitlab-code-suggestions-language-server-experiment` binary on your filesystem and and have registered a Personal Access Token.
1. Install the [LSP package](https://lsp.sublimetext.io/). You can install it from Sublime Text by selecting: `Tools > Command Palette` and typing `Install Package` and then, `LSP` in the command palette menu.
1. Add the following configuration to your `LSP.sublime-settings` file. You can go to the LSP configuration by selecting: `Sublime > Settings > Package Settings > LSP` in the menu.

```json
{
  "clients": {
    "gitlab-code-suggestions-language-server-experiment": {
      "enabled": true,
      "command": [
        "opt/gitlab-code-suggestions-language-server-experiment",
        "serve",
        "--name",
        "gitlab-code-suggestions-language-server-experiment",
        "--srcdir",
        "$file",
        "--timeout-seconds",
        "10",
      ],
      "selector": "source.python"
    }
  }
}
```
- You can adjust the file types for which AI Code Suggestions should be provided by adding them to the `selector`.
- You can update the location of `gitlab-code-suggestions-language-server-experiment` by adding the path to the binary to the `command` (if it's different on your file system).

**Troubleshooting** 

- Re-enable the package it by running `LSP: Enable Language Server in Project` in the command palette and selecting `gitlab-code-suggestions-language-server-experiment` in the menu. 
- Run `LSP: Toggle Log Panel` in the command palette to see any errors or issues with your configuration.
- If you're seeing the `no config found` error when trying to enable the language server, remove the LSP settings and reinstall the package. 

Beta users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations)
