# GitLab Code Suggestions Language Server (Experiment)

This is an experiment to provide [GitLab AI Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html) into [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/) enabled IDEs. This is an **experiment** and is subject to the guidelines in our [documentation](https://docs.gitlab.com/ee/policy/alpha-beta-support.html#experiment).

All feedback should be directed to [this issue](https://gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/-/issues/2).

[[_TOC_]]

## Introduction

LSP is a technology that provides an abstraction layer between tools that
provide analysis results (language servers) and the "consumer" IDEs (language
clients). It provides a generic interface to provide analysis results in
LSP-enabled IDEs so that the analysis only has to be implemented once while
multiple IDEs can benefit from the analysis.

This is an LSP-based language-server that serves [GitLab AI Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html) to LSP enabled IDEs.

The communication is implemented according to the documentation laid out in the
[API docs](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/tree/main)

This language server leverages the [textdocument/completion](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_completion) LSP method to serve code suggestion. For most editors this hooks into the intelliSense functionality which can be often invoked by means of the `[Ctrl]+[SPACE]` `[Ctrl]+[N]` shortcuts.

This language server includes an auto-redaction feature that filters out
secrets client-side so that they are not sent over to the completion API as part of a
code-completion request.

## User Documentation

1. Make sure you have Code Suggestions enabled in your [profile](https://gitlab.com/-/profile/preferences).
1. Download the language server binary from the [Releases
Page](https://gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/-/releases). For every release you can download the binary that matches your os and architecture (binaries are suffixed with `<os>-<arch>` from the list of assets linked in the release.
1. Place the binary anywhere on your filesystem. The configuration in this documentation assumes that the binary is located under `/opt/gitlab-code-suggestions-language-server-experiment`.
1. The language server requires an access token to authenticate with GitLab's API. Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (PAT) with the `read_api` scope.
1. Make the binary executable using the following command in your terminal: `chmod +x /opt/gitlab-code-suggestions-language-server-experiment`. Note: if you're on a Mac, you'll get a warning dialog about opening an app by an identified developer. You'll need to go into your `System Settings > Privacy` to allow the binary to run.
1. Copy your Personal Access Token and register it with the binary using the following command in a terminal:

```console
$ opt/gitlab-code-suggestions-language-server-experiment gitlab-token register
Enter GitLab access token: glpat-ExampleTokenxyz123
✔ wrote GitLab access token to /Users/<username>/Library/Application Support/gitlab-ls/token
```
1. Next, apply the text-editor configuration settings (see below).

### Editor configuration

All the following sample language-client configurations do automatically invoke the
language server (if not already running) and communicate through STDIO.
Configuration that has to be provided by the user is highlighted in brackets
`<user-provided-information>`.

For most editors/client-configurations it is sufficient to tell the language client how the server can be
invoked/started.

- [NeoVim sample configuration](docs/nvim.md)
- [Sublime Text sample configuration](docs/sublime.md)
- [Emacs sample configuration](docs/emacs.md)

## Developer Documentation

For debugging the language server or when integrating new feature, it is
usually a bit easier to run the server in TCP mode so that you can access the
log while using the IDE. In this section, we will discuss how you can setup a
development environment to debug issues or implement new features.

To release a new artifact for this project:

1. Add release notes above the previous release in [CHANGELOG.md](./CHANGELOG.md):
   ```markdown
   ## v1.2.3
   - Add token check command (!32)

   ## v1.2.2
   ```
2. Merge the changes into the `main` branch.
3. Run [this project's pipeline](https://gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/-/pipelines/new) with `CREATE_GIT_TAG` CI/CD variable set.

### Building the project

After cloning this repository you can just build the project by running the
command below.

```bash
go build
```

### Running the Language Server

You can run the server by invoking the command below. For testing purposes it
it may make sense to go with larger timeouts. If you specify a `host` variable,
the language server will automatically switch from the (default) STDIO mode to
TCP mode.

For debugging purposes, it is recommended to run the server in TCP mode as you
can spawn it independently of the client and observe the log output.

```bash
# this variable is used to authenticate against the GitLab API
export LANGSRV_GITLAB_API_TOKEN="<gitlab-api-token>"
/opt/gitlab-code-suggestions-language-server-experiment --name "test" --host "localhost" --srcdir <test-project-dir> --timeout-seconds 100
```
